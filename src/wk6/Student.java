package wk6;

public class Student {
    private double gpaPoints;
    private int creditsTaken;
    private String firstName;
    private String lastName;

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        // return this; // It's doing this at the end of the method call, but don't
                        // write it explicitly.
    }

    public double getGpa() {
        return gpaPoints/creditsTaken;
    }

    public void display() {
        System.out.println(firstName + " " + lastName + " has a gpa of " + getGpa());
    }

    public void recordGrade(String grade, int numCredits) {
        double gpa;
        switch (grade) {
            case "A":
                gpa = 4.0;
                break;
            case "AB":
                gpa = 3.5;
                break;
            case "B":
                gpa = 3.0;
                break;
            case "BC":
                gpa = 2.5;
                break;
            case "C":
                gpa = 2.0;
                break;
            case "CD":
                gpa = 1.5;
                break;
            case "D":
                gpa = 1.0;
                break;
            case "F":
                gpa = 0.0;
                break;
            default:
                gpa = -1.0;
        }
        // more work to actually find gpa
        if(gpa>=0) {
            gpaPoints += gpa * numCredits;
            creditsTaken += numCredits;
        } else {
            System.out.println("Your garbage ignored");
        }
    }
}




