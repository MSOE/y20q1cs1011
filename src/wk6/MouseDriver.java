package wk6;


import java.util.Scanner;

public class MouseDriver {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("How many mice do you want?");
        int desiredMice = in.nextInt();
        for(int mouseNum=0; mouseNum<desiredMice; mouseNum+=1) {
            System.out.println("Enter a name and growth rate");
            String name = in.next();
            double rate = in.nextDouble();
            Mouse mouse = new Mouse();
            mouse.setName(name);
            mouse.setGrowthRate(rate);
            for (int i = 0; i < 52; i += 1) {
                mouse.grow(7);
                mouse.display();
            }
        }
    }
}
