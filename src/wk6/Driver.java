package wk6;

public class Driver {
    public static void main(String[] args) {
        Student josh = new Student("Josh", "Rist");
        josh.recordGrade("A", 4);
        josh.recordGrade("D", 3);
        josh.recordGrade("B", 4);
        josh.recordGrade("C", 4);
        josh.recordGrade("AB", 4);
        josh.recordGrade("D", 3);
        System.out.println(josh.getGpa());

        josh.display();
    }
}
