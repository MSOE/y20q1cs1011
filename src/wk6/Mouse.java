package wk6;

public class Mouse {
    private int daysOld;
    private double weight;
    private double growthRate;
    private String name;

    public Mouse() {
        daysOld = 0;
        weight = 1;
        growthRate = 1.01;
        name = "Fred";
    }

    public void setName(String name) {
        this.name = name;
    }

    public void grow(int numDays) {
        for(int i=0; i<numDays; i+=1) {
            grow();
        }
    }

    public double getWeight() {
        return weight;
    }

    public int getAge() {
        return daysOld;
    }

    public double getGrowthRate() {
        return growthRate;
    }

    public void setGrowthRate(double rate) {
        growthRate = rate;
    }

    public void grow() {
        daysOld += 1;
        weight *= growthRate;
    }

    public void display() {
        System.out.println(name + " is a " + daysOld + " day old mouse weighing " + weight + " ounces.");
    }
}
