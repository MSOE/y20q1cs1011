package wk3;

import java.util.Scanner;

public class Quiz3 {
    public static void main(String[] args) {
        // h - left, j - down, k - up, l - right.
        Scanner in = new Scanner(System.in);
        char directionChoice = in.next().charAt(0);
        while(directionChoice=='h' || directionChoice=='j' || directionChoice=='k' || directionChoice=='l') {
            switch (directionChoice) {
                case 'h':
                    System.out.println("left");
                    break;
                case 'j':
                    System.out.println("down");
                    break;
                case 'k':
                    System.out.println("up");
                    break;
                case 'l':
                    System.out.println("right");
                    break;
            }
            directionChoice = in.next().charAt(0);
        }
    }
}
