package wk5;

public class Complex {

    private double real;
    private double imag;

    public Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public double getAngle() {
        System.out.println(this);
        return 45;
    }

    public double getMagnitude() {
        return Math.sqrt(real*real + imag*imag);
    }

}
