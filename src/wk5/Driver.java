package wk5;

import us.msoe.cs1011.Complex;

public class Driver {
    public static void main(String[] args) {
        Complex num = new Complex(1, 3);
        Complex num2 = new Complex(3, 3);
        Complex num3 = num.plus(num2);
        System.out.println(num.toString());
        System.out.println(num2.toString());
        System.out.println(num3.toString());
        Complex.setPolar();
        System.out.println(num.toString());
        System.out.println(num2.toString());
    }
}
