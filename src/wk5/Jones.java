package wk5;
import java.util.Scanner;

public class Jones {
    public static void main(String[] args) {
        System.out.println("Enter a phrase");
        Scanner in = new Scanner(System.in);
        String phrase = in.nextLine();
        boolean isDigitPresent = false;
        int whitespaceCount = 0;
        int letterCount = 0;
        for(int i=0; i<phrase.length(); i++) {
            if(Character.isWhitespace(phrase.charAt(i))) {
                whitespaceCount++;
            } else if(Character.isLetter(phrase.charAt(i))) {
                letterCount++;
            } else if(Character.isDigit(phrase.charAt(i))) {
                isDigitPresent = true;
            }
        }
        System.out.println("Whitespace count: " + whitespaceCount);
        System.out.println("Letter count: " + letterCount);
        if(isDigitPresent) {
            System.out.println("with digits");
        }
    }






}
