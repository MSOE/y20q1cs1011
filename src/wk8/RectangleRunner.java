package wk8;

public class RectangleRunner {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(3.1, 4.1);
        Rectangle rect2 = new Rectangle("3 x 4");
        Rectangle rect3 = new Rectangle();
        System.out.println(rect1 + " area: " + rect1.getArea() + " perim: " + rect1.getPerimeter());
        System.out.println(rect2.getWidth() + " x " + rect2.getHeight() + " area: " + rect2.getArea() + " perim: " + rect2.getPerimeter());
        System.out.println(rect3.getWidth() + " x " + rect3.getHeight() + " area: " + rect3.getArea() + " perim: " + rect3.getPerimeter());
        boolean result = rect1.setHeight(8.1);
        if(!result) {
            System.out.println("Error on return of setHeight()");
        }
        double h = rect2.getHeight();
        if(Math.abs(h-8.1)<0.00001) {
            System.out.println("Error either setting or getting height");
        }
        result = rect2.setWidth(-0.1);
        if(result) {
            System.out.println("Error: setWidth() should not return true if passed invalid width");
        }
        double w = rect1.getWidth();
        if(h==-0.1) {
            System.out.println("Error: setWidth() should not have changed the width to an invalid value");
        }
        double a = rect3.getArea();
        double p = rect3.getPerimeter();
    }
}
