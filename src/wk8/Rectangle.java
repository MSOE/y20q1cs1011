package wk8;

import java.util.Scanner;

public class Rectangle {
    private double width;
    private double height;

    public Rectangle(double width, double height) {
        if(width<=0 || height<=0) {
            this.width = 1.0;
            this.height = 1.0;
        } else {
            this.width = width;
            this.height = height;
        }
    }

    // "3.1 x 3.1"
    public Rectangle(String dimensions) {
        Scanner parser = new Scanner(dimensions);
        width = parser.nextDouble();
        parser.next();
        height = parser.nextDouble();
    }

    public Rectangle() {
        this(Math.random()*9.9+0.1, Math.abs(Math.random()*5-5));
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }

    public boolean setWidth(double width) {
        boolean changed = false;
        if(width>0 && this.width!=width) {
            this.width = width;
            changed = true;
        }
        return changed;
    }

    public boolean setHeight(double height) {
        boolean changed = false;
        if(height>0 && height!=this.height) {
            this.height = height;
            changed = true;
        }
        return changed;
    }

    public double getArea() {
        return width * height;
    }

    public double getPerimeter() {
        return 2 * (width + height);
    }

    @Override
    public String toString() {
        return "Rectangle{" + width +
                " x " + height + '}';
    }

    public boolean equals(Rectangle rect) {
        return Math.abs(this.getArea()-rect.getArea())<0.0001;
    }
}
