package wk8;

import java.util.Scanner;

public class ArrayDriver {
    public static void main(String[] args) {
        Rectangle[] rectangles = new Rectangle[0];
        for(int i=0; i<10; i++) {
            rectangles = addRectangle(rectangles);
        }
        for(int i=0; i<rectangles.length; i++) {
            System.out.println(rectangles[i]);
        }
    }

    private static Rectangle[] addRectangle(Rectangle[] rectangles) {
        Rectangle[] biggerArray = new Rectangle[rectangles.length+1];
        for(int i=0; i<rectangles.length; i++) {
            biggerArray[i] = rectangles[i];
        }
        biggerArray[biggerArray.length-1] = new Rectangle();
        return biggerArray;
    }

    public static void main2(String[] args) {
        double[] grades = getGrades();
        printArray(grades);
        int[] numbers = {0, 1, 2, 3, 4, 5, 6};
        printArray(numbers);
        for(int i=0; i<numbers.length; i++) {
            numbers[i] = numbers[i] * numbers[i];
        }
        printArray(numbers);
        int[] trouble = {};
        printArray(trouble);
    }

    private static double[] getGrades() {
        Scanner in = new Scanner(System.in);
        System.out.println("How many grades do you want to enter?");
        int count = in.nextInt();
        System.out.println("Enter five grades (as doubles)");
        double[] grades = new double[count];
        for(int i=0; i<grades.length; i++) {
            grades[i] = in.nextDouble();
        }
        return grades;
    }

    private static void printArray(double[] data) {
        System.out.print("[");
        for(int i=0; i<data.length; i++) {
            System.out.print(data[i]);
            if(i<data.length-1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }

    private static void printArray(int[] data) {
        System.out.print("[");
        for(int i=0; i<data.length; i++) {
            System.out.print(data[i]);
            if(i<data.length-1) {
                System.out.print(", ");
            }
        }
        System.out.println("]");
    }
}
