package wk10;

import java.util.ArrayList;

public class Reverse {
    public static void main(String[] args) {
        int[] nums = {1, 2, 2, 3, 5, 3};
        System.out.println(reverse(toArrayList(nums)));
    }

    private static ArrayList<Integer> reverse(ArrayList<Integer> nums) {
        ArrayList<Integer> list = new ArrayList<>(nums.size());
        for(int i=nums.size()-1; i>=0; i--) {
            list.add(nums.get(i));
        }
        return list;
    }

    public static ArrayList<Integer> toArrayList(int[] nums) {
        ArrayList<Integer> list = new ArrayList<>();
        for(int num : nums) {
            list.add(num);
        }
        return list;
    }
}
