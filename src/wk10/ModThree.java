package wk10;

import java.util.ArrayList;

public class ModThree {
    public static void main(String[] args) {
        int[][] tests = {
                // True
                {2, 1, 3, 5},
                {2, 4, 2, 5},
                {9, 9, 9},
                {9, 7, 2, 9, 2, 2, 6},
                // False
                {2, 1, 2, 5},
                {1, 2, 1, 2, 1},
                {1, 2, 1},
                {1, 2},
                {1},
                {},
                {9, 7, 2, 9},
                {9, 7, 2, 9, 2, 2},
        };
        if(!modThree(Reverse.toArrayList(tests[0]))) {
            System.err.println("Problems with " + Reverse.toArrayList(tests[0]));
        }
        if(!modThree(Reverse.toArrayList(tests[1]))) {
            System.err.println("Problems with " + Reverse.toArrayList(tests[1]));
        }
        if(!modThree(Reverse.toArrayList(tests[2]))) {
            System.err.println("Problems with " + Reverse.toArrayList(tests[2]));
        }
        if(!modThree(Reverse.toArrayList(tests[3]))) {
            System.err.println("Problems with " + Reverse.toArrayList(tests[3]));
        }
        for(int i=5; i<tests.length; i++) {
            if (modThree(Reverse.toArrayList(tests[i]))) {
                System.err.println("Problems with " + Reverse.toArrayList(tests[i]));
            }
        }
    }

    private static boolean modThree(ArrayList<Integer> nums) {
        boolean threeInARow = false;
        for(int i=0; !threeInARow && i<nums.size()-2; i++) {
            threeInARow = nums.get(i)%2==nums.get(i+1)%2 && nums.get(i+1)%2==nums.get(i+2)%2;
        }
        return threeInARow;
    }

}
