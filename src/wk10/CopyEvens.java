package wk10;

import java.util.ArrayList;

public class CopyEvens {
    public static void main(String[] args) {
        int[] test1 = {3, 2, 4, 5, 8};
        int count1 = 2;
        int[] answer1 = {2, 4};
        int[] test2 = {3, 2, 4, 5, 8};
        int count2 = 3;
        int[] answer2 = {2, 4, 8};
        ArrayList<Integer> result1 = copyEvens(Reverse.toArrayList(test1), count1);
        if (!result1.equals(Reverse.toArrayList(answer1))) {
            System.err.println("Problems with " + Reverse.toArrayList(test1) + " with count: " + count1);
            System.err.println(result1);
        }
        ArrayList<Integer> result2 = copyEvens(Reverse.toArrayList(test2), count2);
        if (!result2.equals(Reverse.toArrayList(answer2))) {
            System.err.println("Problems with " + Reverse.toArrayList(test2) + " with count: " + count2);
            System.err.println(result2);
        }
    }

    private static ArrayList<Integer> copyEvens(ArrayList<Integer> nums, int count) {
        ArrayList<Integer> evens = new ArrayList<>();
        for(int i=0; i<nums.size() && evens.size()<count; i++) {
            if(nums.get(i)%2==0) {
                evens.add(nums.get(i));
            }
        }
        return evens;
    }
}
